using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace project_music_be.Models
{
    public class CartsModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ScheduleId { get; set; }
        public string? Image { get; set; }
        public string? Title { get; set; }
        public int Price { get; set; }
        public string? Category { get; set; }
        public DateTime Schedule { get; set;}
        public bool IsSelected { get; set; }
    }
}