namespace project_music_be.Models
{
    public class InvoiceModel
    {   
        public string? NoInvoice { get; set; }
        public DateTime CreatedAt { get; set;}
        public int TotalOrders { get; set; }
        public int TotalPrice { get; set; }

    }
}