using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace project_music_be.Models
{
    public class DetailInvoiceModel
    {
        public string? NoInvoice { get; set; }
        public DateTime? CreateAt { get; set; }
        public int TotalPrice { get; set; }
        public List<InvoiceListModel>? ListInvoice { get; set; }
    }
}