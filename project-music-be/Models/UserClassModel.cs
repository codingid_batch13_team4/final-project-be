namespace project_music_be.Models
{
    public class UserClassModel
    {   
        public string? Image { get; set; }
        public string? Category { get; set; }
        public string? Title { get; set; }
        public DateTime Schedule { get; set;}

    }
}