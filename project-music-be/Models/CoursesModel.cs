namespace project_music_be.Models
{
    public class CoursesModel
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Image {get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public int CategoryId { get; set; }
        public string? CategoryName { get; set; } 
        public int Price { get; set; }
        public bool IsActive { get; set; }
    }
}