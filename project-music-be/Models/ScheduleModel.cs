namespace project_music_be.Models
{
    public class ScheduleModel
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string? TitleCourse { get; set; }
    }
}