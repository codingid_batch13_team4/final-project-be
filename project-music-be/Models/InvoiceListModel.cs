using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace project_music_be.Models
{
    public class InvoiceListModel
    {
        public string? Course { get; set; }
        public string? Category { get; set; }
        public DateTime Shedule { get; set; }
        public int Price { get; set; }
    }
}