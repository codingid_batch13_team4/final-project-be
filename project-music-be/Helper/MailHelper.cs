using MailKit.Net.Smtp;

using MailKit.Security;
using MimeKit;

namespace project_music_be.Helper
{
    public static class MailHelper
    {
        public static async Task Send(string toEmail, string subject, string messageText)
        {
            /* configuration */
            var message = new  MimeMessage();
            message.From.Add( new MailboxAddress("No Reply", "projectmusictrial@gmail.com"));
            message.To.Add(new MailboxAddress("User", toEmail));
            message.Subject = subject;
            message.Importance = MessageImportance.High;
            message.Priority = MessagePriority.Urgent;
            message.XPriority = XMessagePriority.Highest;
            /* configuration */

            /* masukin message text */
            message.Body = new TextPart("html")
            {
                Text = messageText
            };
            /* masukin message text */

            /* kirim emailnya */
            using (var smtp = new SmtpClient())
            {
                await smtp.ConnectAsync("smtp.gmail.com", 587, SecureSocketOptions.StartTls);
                await smtp.AuthenticateAsync("projectmusictrial@gmail.com", "yyuhkgwsyqlosrid");
                await smtp.SendAsync(message);
                await smtp.DisconnectAsync(true);
            }
            /* kirim emailnya */
        }

        // internal static Task Send(string email, string v)
        // {
        //     throw new NotImplementedException();
        // }
    }
}