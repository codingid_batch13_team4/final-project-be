using MySql.Data.MySqlClient;
using project_music_be.DTO;
using project_music_be.Models;

namespace project_music_be.Repositories
{
    public class InvoiceRepository
    {
        private readonly string _connectionString = string.Empty;

        public InvoiceRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }

        public bool InsertInvoice(List<CheckInvoiceModel> carts, InvoiceDTO data, int userId)
        {
            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();
                using (MySqlTransaction transaction = conn.BeginTransaction())
                {
                    try
                    {
                        string countQuery =
                            "SELECT COUNT(DISTINCT number_invoice) AS total_count FROM invoice WHERE fk_id_user = @userId;";
                        using (var countCmd = new MySqlCommand(countQuery, conn))
                        {
                            countCmd.Parameters.AddWithValue("@userId", userId);
                            int invoiceCount = Convert.ToInt32(countCmd.ExecuteScalar());
                            string nextInvoiceNumber = (invoiceCount + 1).ToString("D3");
                            string userFormat = userId.ToString("D4");
                            string invoiceNum = $"APM-ID{userFormat}-{nextInvoiceNumber}";

                            string sqlInsert =
                                "INSERT INTO invoice VALUES (DEFAULT, @NoInvoice, @CreateAt, @ScheduleId, @IdUser, @IdPayment)";
                            string sqlDelete =
                                "DELETE FROM cart WHERE fk_id_user = @userId AND is_selected = TRUE";

                            foreach (CheckInvoiceModel cart in carts)
                            {
                                using (var cmd = new MySqlCommand(sqlInsert, conn))
                                {
                                    cmd.Parameters.AddWithValue("@NoInvoice", invoiceNum);
                                    cmd.Parameters.AddWithValue("@CreateAt", data.CreateAt);
                                    cmd.Parameters.AddWithValue("@ScheduleId", cart.ScheduleId);
                                    cmd.Parameters.AddWithValue("@IdUser", userId);
                                    cmd.Parameters.AddWithValue("@IdPayment", data.PaymentId);

                                    cmd.Transaction = transaction;
                                    cmd.ExecuteNonQuery();
                                }
                            }

                            using (var deleteCmd = new MySqlCommand(sqlDelete, conn))
                            {
                                deleteCmd.Parameters.AddWithValue("@userId", userId);
                                deleteCmd.Transaction = transaction;
                                deleteCmd.ExecuteNonQuery();
                            }

                            transaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Console.WriteLine($"Error: {ex.Message}");
                        return false;
                    }
                }
            }
            return true;
        }

        public bool InsertDirectInvoice(int ScheduleId, InvoiceDTO data, int userId)
        {
            using MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();
                string countQuery =
                    "SELECT COUNT(DISTINCT number_invoice) AS total_count FROM invoice WHERE fk_id_user = @userId;";

                using (var countCmd = new MySqlCommand(countQuery, conn))
                {
                    countCmd.Parameters.AddWithValue("@userId", userId);
                    int invoiceCount = Convert.ToInt32(countCmd.ExecuteScalar());
                    string nextInvoiceNumber = (invoiceCount + 1).ToString("D3");
                    string userFormat = userId.ToString("D4");
                    string invoiceNum = $"APM-ID{userFormat}-{nextInvoiceNumber}";

                    MySqlCommand cmd = new MySqlCommand(
                        "INSERT INTO invoice VALUES (DEFAULT, @NoInvoice, @CreateAt, @ScheduleId, @IdUser, @IdPayment)",
                        conn
                    );

                    cmd.Parameters.AddWithValue("@NoInvoice", invoiceNum);
                    cmd.Parameters.AddWithValue("@CreateAt", data.CreateAt);
                    cmd.Parameters.AddWithValue("@ScheduleId", ScheduleId);
                    cmd.Parameters.AddWithValue("@IdUser", userId);
                    cmd.Parameters.AddWithValue("@IdPayment", data.PaymentId);

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                conn.Close();
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }

        public List<InvoiceModel> GetInvoiceById(int userId)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            List<InvoiceModel> result = new List<InvoiceModel>();

            try
            {
                conn.Open();
                string sql =
                    "SELECT invoice.number_invoice, invoice.created_at, COUNT(invoice.number_invoice) AS total_product, SUM(product.price) AS totalPrice FROM invoice JOIN schedule ON invoice.fk_id_schedule = schedule.id_schedule JOIN product ON product.id_product = schedule.fk_id_product WHERE invoice.fk_id_user = @IdUser GROUP BY invoice.number_invoice ORDER BY invoice.number_invoice DESC;";
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@IdUser", userId);
                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    result.Add(
                        new InvoiceModel
                        {
                            NoInvoice = reader.GetString(0),
                            CreatedAt = DateTime.Parse(reader.GetString(1)),
                            TotalOrders = reader.GetInt32(2),
                            TotalPrice = reader.GetInt32(3)
                        }
                    );
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            conn.Close();

            return result;
        }

        public List<InvoiceModel> GetInvoiceAll()
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            List<InvoiceModel> result = new List<InvoiceModel>();

            try
            {
                conn.Open();
                string sql =
                    "SELECT invoice.number_invoice, invoice.created_at, COUNT(invoice.number_invoice) AS total_product, SUM(product.price) AS totalPrice FROM invoice JOIN schedule ON invoice.fk_id_schedule = schedule.id_schedule JOIN product ON product.id_product = schedule.fk_id_product GROUP BY invoice.number_invoice ORDER BY invoice.number_invoice DESC;";
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    result.Add(
                        new InvoiceModel
                        {
                            NoInvoice = reader.GetString(0),
                            CreatedAt = DateTime.Parse(reader.GetString(1)),
                            TotalOrders = reader.GetInt32(2),
                            TotalPrice = reader.GetInt32(3)
                        }
                    );
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            conn.Close();

            return result;
        }

        public List<UserClassModel> GetMyClass(int userId)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            List<UserClassModel> result = new List<UserClassModel>();

            try
            {
                conn.Open();
                string sql =
                    "SELECT product.image, product.NAME, category.name, schedule.schedule_date FROM invoice JOIN schedule ON invoice.fk_id_schedule = schedule.id_schedule JOIN product ON product.id_product = schedule.fk_id_product JOIN category ON category.id_category = product.fk_id_category WHERE invoice.fk_id_user = @IdUser ORDER BY schedule.schedule_date ASC;";
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@IdUser", userId);
                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    result.Add(
                        new UserClassModel
                        {
                            Image = reader.GetString(0),
                            Title = reader.GetString(1),
                            Category = reader.GetString(2),
                            Schedule = DateTime.Parse(reader.GetString(3))
                        }
                    );
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            conn.Close();

            return result;
        }

        public DetailInvoiceModel GetDetailInvoice(string InvoiceNumber, int userId)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            DetailInvoiceModel result = new DetailInvoiceModel();
            List<InvoiceListModel> temp = new List<InvoiceListModel>();
            try
            {
                conn.Open();
                string sql =
                    "SELECT invoice.number_invoice, invoice.created_at, SUM(product.price) AS totalPrice , product.NAME, category.name, schedule.schedule_date, product.price FROM invoice JOIN schedule ON invoice.fk_id_schedule = schedule.id_schedule JOIN product ON product.id_product = schedule.fk_id_product JOIN category ON category.id_category = product.fk_id_category WHERE invoice.fk_id_user = @IdUser AND invoice.number_invoice = @Number GROUP BY invoice.number_invoice, invoice.created_at, product.id_product,schedule.id_schedule;";
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@Number", InvoiceNumber);
                cmd.Parameters.AddWithValue("@IdUser", userId);
                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    result.NoInvoice = reader.GetString(0);
                    result.CreateAt = DateTime.Parse(reader.GetString(1));
                    result.TotalPrice = reader.GetInt32(2);

                    temp.Add(
                        new InvoiceListModel
                        {
                            Course = reader.GetString(3),
                            Category = reader.GetString(4),
                            Shedule = DateTime.Parse(reader.GetString(5)),
                            Price = reader.GetInt32(6)
                        }
                    );
                }
                result.ListInvoice = temp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            conn.Close();

            return result;
        }

        public DetailInvoiceModel GetDetailInvoiceAdmin(string InvoiceNumber)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            DetailInvoiceModel result = new DetailInvoiceModel();
            List<InvoiceListModel> temp = new List<InvoiceListModel>();
            try
            {
                conn.Open();
                string sql =
                    "SELECT invoice.number_invoice, invoice.created_at, SUM(product.price) AS totalPrice , product.NAME, category.name, schedule.schedule_date, product.price FROM invoice JOIN schedule ON invoice.fk_id_schedule = schedule.id_schedule JOIN product ON product.id_product = schedule.fk_id_product JOIN category ON category.id_category = product.fk_id_category WHERE invoice.number_invoice = @Number GROUP BY invoice.number_invoice, invoice.created_at, product.id_product,schedule.id_schedule;";
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@Number", InvoiceNumber);
                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    result.NoInvoice = reader.GetString(0);
                    result.CreateAt = DateTime.Parse(reader.GetString(1));
                    result.TotalPrice = reader.GetInt32(2);

                    temp.Add(
                        new InvoiceListModel
                        {
                            Course = reader.GetString(3),
                            Category = reader.GetString(4),
                            Shedule = DateTime.Parse(reader.GetString(5)),
                            Price = reader.GetInt32(6)
                        }
                    );
                }
                result.ListInvoice = temp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            conn.Close();

            return result;
        }

        public List<CheckInvoiceModel> GetUnlistedCourse(InvoiceDTO data, int userId)
        {
            List<CheckInvoiceModel> result = new List<CheckInvoiceModel>();
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();
                string sql =
                    "SELECT cart.fk_id_schedule FROM cart JOIN schedule ON cart.fk_id_schedule = schedule.id_schedule WHERE cart.fk_id_user = @IdUser AND cart.is_selected = TRUE";

                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@IdUser", userId);

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    CheckInvoiceModel orderModel = new CheckInvoiceModel
                    {
                        ScheduleId = reader.GetInt32(0),
                    };

                    result.Add(orderModel);
                }

                // Check if any fk_id_schedule in the result exists in the "invoice"
                if (result.Any(item => IsScheduleInInvoice(item.ScheduleId, userId)))
                {
                    // If any schedule exists in the "invoice," return an empty list
                    result.Clear();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                conn.Close();
            }

            return result;
        }

        public bool IsScheduleInInvoice(int scheduleId, int userId)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();
                string sql =
                    "SELECT COUNT(*) FROM invoice WHERE fk_id_schedule = @ScheduleId AND fk_id_user = @IdUser";

                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@ScheduleId", scheduleId);
                cmd.Parameters.AddWithValue("@IdUser", userId);

                int count = Convert.ToInt32(cmd.ExecuteScalar());

                return count > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            finally
            {
                conn.Close();
            }
        }

        public bool IsScheduleInCart(int scheduleId, int userId)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();
                string sql =
                    "SELECT COUNT(*) FROM cart WHERE fk_id_schedule = @ScheduleId AND fk_id_user = @IdUser";

                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@ScheduleId", scheduleId);
                cmd.Parameters.AddWithValue("@IdUser", userId);

                int count = Convert.ToInt32(cmd.ExecuteScalar());

                return count > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
