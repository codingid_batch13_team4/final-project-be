using MySql.Data.MySqlClient;
using Org.BouncyCastle.Bcpg.Attr;
using project_music_be.DTO;
using project_music_be.Models;

namespace project_music_be.Repositories
{
    public class PaymentRepository
    {
        private readonly string _connStr;
        public PaymentRepository(IConfiguration configuration)
        {
            _connStr = configuration.GetConnectionString("Default");
        }
        public bool InsertPayment(PaymentDTO data, string fileUrlPath)
        {
            MySqlConnection conn = new MySqlConnection(_connStr);

            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("INSERT INTO payment VALUE(DEFAULT, @Name, @Image, @Status)", conn);

                cmd.Parameters.AddWithValue("@Name", data.Name);
                cmd.Parameters.AddWithValue("@Image", fileUrlPath);
                cmd.Parameters.AddWithValue("@Status", data.Status);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                Console.WriteLine(ex.Message);
                return false;
            }
            conn.Close();

            return true;
        }

        public List<PaymentModel> GetListPayment (bool? status)
        {
            MySqlConnection conn = new MySqlConnection(_connStr);

            List<PaymentModel> result = new List<PaymentModel>();

            try
            {
                conn.Open();
                string sql = "";
                if (status == null)
                {
                    sql = "SELECT * FROM payment";
                }
                else
                {
                    sql = "SELECT * FROM payment WHERE STATUS = @Status";
                }

                MySqlCommand cmd = new MySqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@Status",status);

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    int id = reader.GetInt32("id_payment");
                    string name = reader.GetString("name");
                    string image = reader.GetString("image");
                    bool isActive = reader.GetBoolean("status");
                

                    result.Add(new PaymentModel
                    {
                        Id = id,
                        Name = name,
                        Image = image,
                        IsActive = isActive
                    });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            conn.Close();
            Console.WriteLine("Done.");

            return result;
        }

        public PaymentModel  GetPaymentId(int id)
        {
            MySqlConnection conn = new MySqlConnection(_connStr);

            PaymentModel result = new PaymentModel();

            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM `payment` WHERE id_payment = @Id",conn);

                cmd.Parameters.AddWithValue("@Id", id);

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    result.Id = reader.GetInt32("id_payment");
                    result.Name = reader.GetString("name");
                    result.Image = reader.GetString("image");
                    result.IsActive = reader.GetBoolean("status");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            conn.Close();
            Console.WriteLine("Done.");

            return result;
        }

        public PaymentModel?  CheckPayment(PaymentDTO data)
        {
            MySqlConnection conn = new MySqlConnection(_connStr);

            PaymentModel? result = null;

            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM `payment` WHERE name = @Name",conn);

                cmd.Parameters.AddWithValue("@Name", data.Name);

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result = new PaymentModel()
                    {
                        Id = reader.GetInt32("id_payment"),
                        Name = reader.GetString("name"),
                        Image = reader.GetString("image"),
                        IsActive = reader.GetBoolean("status")
                    };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            conn.Close();
            Console.WriteLine("Done.");

            return result;
        }

        public bool UpdatePayment(PaymentDTO data, string fileUrlPath, int id)
        {
            MySqlConnection conn = new MySqlConnection(_connStr);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("UPDATE payment SET name=@Name, image=@Image, status=@IsActive WHERE id_payment = @Id", conn);

                cmd.Parameters.AddWithValue("@Id",id);
                cmd.Parameters.AddWithValue("Name", data.Name);
                cmd.Parameters.AddWithValue("@Image", fileUrlPath);
                cmd.Parameters.AddWithValue("@IsActive", data.Status);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                Console.WriteLine(ex.Message);
                return false;
            }
            conn.Close();

            return true;
        }

        public bool UpdateStatusPayment(int id, bool status)
        {
            MySqlConnection conn = new MySqlConnection(_connStr);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("UPDATE payment SET status = @IsActive WHERE id_payment = @Id", conn);

                cmd.Parameters.AddWithValue("@Id",id);
                cmd.Parameters.AddWithValue("@IsActive", status);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex )
            {
                conn.Close();
                Console.WriteLine(ex.Message);
                return false;
            }
            conn.Close();

            return true;
        }
    }
}