using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using project_music_be.DTO;
using project_music_be.Models;

namespace project_music_be.Repositories
{
    public class CategoryRepository
    {
        private readonly string _connStr;
        public CategoryRepository(IConfiguration configuration)
        {
            _connStr = configuration.GetConnectionString("Default");
        }
        public bool InsertCategory(CategoryDTO data, string fileUrlPath)
        {
            MySqlConnection conn =  new MySqlConnection(_connStr);

            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("INSERT INTO category VALUE(DEFAULT, @Name, @Description, @Image, @Status)",conn);

                cmd.Parameters.AddWithValue("@Name", data.Name);
                cmd.Parameters.AddWithValue("@Description", data.Description);
                cmd.Parameters.AddWithValue("@Image", fileUrlPath);
                cmd.Parameters.AddWithValue("@Status", data.Status);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                Console.WriteLine(ex.Message);
                return false;
            }
            conn.Close();

            return true;
        }

        public List<CategoryModel> GetListCategory(bool? status)
        {
            MySqlConnection conn = new MySqlConnection(_connStr);

            List<CategoryModel> result = new List<CategoryModel>();

            try
            {
                conn.Open();
                string sql= "";
                if (status == null)
                {
                    sql = "SELECT * FROM category";
                }
                else
                {
                    sql = "SELECT * FROM category WHERE STATUS = @Status";
                }   

                MySqlCommand cmd = new MySqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@Status", status);

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    int id = reader.GetInt32("id_category");
                    string name = reader.GetString("name");
                    string description = reader.GetString("description");
                    string image = reader.GetString("image");
                    bool isActive = reader.GetBoolean("STATUS");
                

                    result.Add(new CategoryModel
                    {
                        Id = id,
                        Name = name,
                        Description = description,
                        Image = image,
                        IsActive = isActive
                    });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            conn.Close();
            Console.WriteLine("Done.");

            return result;
        }

        public CategoryModel GetCategoryId(int id)
        {
            MySqlConnection conn = new MySqlConnection(_connStr);

            CategoryModel result = new CategoryModel();
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM `category` WHERE id_category=@id",conn);
                cmd.Parameters.AddWithValue("@Id",id);

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    result.Id = reader.GetInt32("id_category");
                    result.Name = reader.GetString("name");
                    result.Description = reader.GetString("description");
                    result.Image = reader.GetString("image");
                    result.IsActive = reader.GetBoolean("STATUS");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            conn.Close();
            Console.WriteLine("Done.");

            return result;
        }

        public CategoryModel? CheckCategory(CategoryDTO data)
        {
            MySqlConnection conn = new MySqlConnection(_connStr);

            CategoryModel? result = null;
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM `category` WHERE NAME=@Name",conn);
                cmd.Parameters.AddWithValue("@Name",data.Name);

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result = new CategoryModel()
                    {
                        Id = reader.GetInt32("id_category"),
                        Name = reader.GetString("name"),
                        Description = reader.GetString("description"),
                        Image = reader.GetString("image"),
                        IsActive = reader.GetBoolean("STATUS")
                    };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            conn.Close();
            Console.WriteLine("Done.");

            return result;
        }

        public bool UpdateCategory(CategoryDTO data, string fileUrlPath, int id)
        {
            MySqlConnection conn = new MySqlConnection(_connStr);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("UPDATE category SET name=@Name, description=@Description, image=@Image, STATUS=@IsActive WHERE id_category = @Id", conn);

                cmd.Parameters.AddWithValue("@Id", id);
                cmd.Parameters.AddWithValue("Name", data.Name);
                cmd.Parameters.AddWithValue("@Description",data.Description);
                cmd.Parameters.AddWithValue("@Image", fileUrlPath);
                cmd.Parameters.AddWithValue("@IsActive", data.Status);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                Console.WriteLine(ex.Message);
                return false;
            }
            conn.Close();

            return true;
        }

        public bool UpdateStatusCategory(int id, bool status)
        {
            MySqlConnection conn = new MySqlConnection(_connStr);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("UPDATE category SET STATUS = @IsActive WHERE id_Category = @Id",conn);

                cmd.Parameters.AddWithValue("@Id",id);
                cmd.Parameters.AddWithValue("@IsActive", status);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                Console.WriteLine(ex.Message);
                return false;
            }
            conn.Close();

            return true;
        }
    }
}