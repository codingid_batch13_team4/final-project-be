using System;
using project_music_be.DTO;
using project_music_be.Models;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;

namespace project_music_be.Repositories
{
    public class CoursesRepository
    {
        private readonly string _connectionString = string.Empty;
        public CoursesRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }

        public bool InsertCourse(CourseDTO data, string fileUrlPath)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();
                string sql = "INSERT INTO product VALUES (DEFAULT, @Name, @Description, @Image, @Price, @status ,@FKIdCategory)";
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@Name", data.TitleCourse);
                cmd.Parameters.AddWithValue("@Description", data.DescriptionCourse);
                cmd.Parameters.AddWithValue("@Price", data.Price);
                cmd.Parameters.AddWithValue("@Image", fileUrlPath);
                cmd.Parameters.AddWithValue("@status", data.IsActive);
                cmd.Parameters.AddWithValue("@FKIdCategory", data.CategoryId);

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                conn.Close();
                Console.WriteLine(ex.Message);
                return false;
            }
            conn.Close();

            return true;
        }

        public List<CoursesModel> GetCourses(int? limit, int? categoryId, int? courseId)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            List<CoursesModel> result = new List<CoursesModel>();
            try
            {
                conn.Open();
                string sql = "";
                if (limit == null && categoryId == null && courseId == null)
                {
                    sql = "SELECT product.id_product, product.NAME, product.image, product.DESCRIPTION, category.id_category, category.NAME as type, product.STATUS, product.price FROM product JOIN category ON category.id_category = product.fk_id_category";
                }
                else if (limit != null && categoryId == null && courseId == null)
                {
                    sql = "SELECT product.id_product, product.NAME, product.image, product.DESCRIPTION, category.id_category, category.NAME as type, product.STATUS, product.price FROM product JOIN category ON category.id_category = product.fk_id_category WHERE product.STATUS = true AND category.STATUS = true ORDER BY product.id_product ASC LIMIT @Limit";
                }
                else if (limit != null && categoryId != null && courseId == null)
                {
                    sql = "SELECT product.id_product, product.NAME, product.image, product.DESCRIPTION, category.id_category, category.NAME as type, product.STATUS, product.price FROM product JOIN category ON category.id_category = product.fk_id_category WHERE product.STATUS = true AND category.id_category = @CategoryId AND category.STATUS = true ORDER BY product.id_product DESC LIMIT @Limit";
                }
                else
                {
                    sql = "SELECT product.id_product, product.NAME, product.image, product.DESCRIPTION, category.id_category, category.NAME as type, product.STATUS, product.price FROM product JOIN category ON category.id_category = product.fk_id_category WHERE product.STATUS = true AND category.id_category = @CategoryId AND product.id_product != @CourseId AND category.STATUS = true ORDER BY product.id_product DESC LIMIT @Limit";
                }

                MySqlCommand cmd = new MySqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@Limit", limit);
                cmd.Parameters.AddWithValue("@CategoryId", categoryId);
                cmd.Parameters.AddWithValue("@CourseId", courseId);

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    int id = reader.GetInt32("id_product");
                    string name = reader.GetString("NAME");
                    string image = reader.GetString("image");
                    string description = reader.GetString("DESCRIPTION");
                    int idCategory = reader.GetInt32("id_category");
                    string type = reader.GetString("type");
                    bool isActive = reader.GetBoolean("STATUS");
                    int price = reader.GetInt32("price");

                    result.Add(new CoursesModel
                    {
                        CategoryId = idCategory,
                        Name = name,
                        Image = image,
                        Description = description,
                        Id = id,
                        CategoryName = type,
                        Price = price,
                        IsActive = isActive
                    });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            conn.Close();

            return result;
        }

        public bool UpdateCourse(CourseDTO data, string fileUrlPath, int id)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();
                string sql = "UPDATE product SET NAME=@Name, DESCRIPTION=@Description,price=@Price,image=@Image,STATUS=@status,fk_id_category = @FKIdCategory WHERE id_product = @Id";

                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@Id", id);
                cmd.Parameters.AddWithValue("@Name", data.TitleCourse);
                cmd.Parameters.AddWithValue("@Description", data.DescriptionCourse);
                cmd.Parameters.AddWithValue("@Price", data.Price);
                cmd.Parameters.AddWithValue("@Image", fileUrlPath);
                cmd.Parameters.AddWithValue("@status", data.IsActive);
                cmd.Parameters.AddWithValue("@FKIdCategory", data.CategoryId);
                
                int rowsAffected = cmd.ExecuteNonQuery();

                if(rowsAffected < 1)
                {
                    throw new Exception("Failed to Update");
                }
            }
            catch (Exception ex)
            {
                conn.Close();
                Console.WriteLine(ex.Message);
                return false;
            }
            conn.Close();

            return true;
        }

        public bool UpdateStatus(bool status, int id)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();
                string sql = "UPDATE product SET STATUS=@status WHERE id_product = @Id";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@Id", id);
                cmd.Parameters.AddWithValue("@status", status);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                Console.WriteLine(ex.Message);
                return false;
            }
            conn.Close();

            return true;
        }

        public CourseDetailModel GetDetailCourse(int id)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            CourseDetailModel result = new CourseDetailModel();
            List<ScheduleModel> schedules = new List<ScheduleModel>();
            try
            {
                conn.Open();
                // string sql = "SELECT product.id_product, product.NAME, product.image, product.DESCRIPTION, category.NAME, category.id_category, product.price FROM product JOIN category ON category.id_category = product.fk_id_category WHERE id_product = @Id ";
                string sql = "SELECT product.id_product, product.NAME, product.image, product.DESCRIPTION, category.NAME, category.id_category, product.price, schedule.id_schedule, schedule.schedule_date FROM schedule JOIN product ON product.id_product = schedule.fk_id_product JOIN category ON category.id_category = product.fk_id_category WHERE product.id_product = @Id";
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@Id", id);
                cmd.ExecuteNonQuery();

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    result.Id = reader.GetInt32(0);
                    result.Name = reader.GetString(1);
                    result.Image = reader.GetString(2);
                    result.Description = reader.GetString(3);
                    result.Category = reader.GetString(4);
                    result.CategoryId = reader.GetInt32(5);
                    result.Price = reader.GetInt32(6);
                    schedules?.Add(new ScheduleModel
                        {
                            Date = DateTime.Parse(reader.GetString(8)),
                            Id = reader.GetInt32(7),
                        });
                }
                result.Schedules = schedules ?? new List<ScheduleModel>();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            }
            conn.Close();

            return result;
        }

        public bool DeleteCourse(int id)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();
                string sql = "DELETE FROM product WHERE id_product = @Id";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@Id", id);

                int rowsAffected = cmd.ExecuteNonQuery();

                return rowsAffected > 0; // If rowsAffected > 0, deletion was successful
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false; // Failed to delete course
            }
            finally
            {
                conn.Close();
            }
        }

        public CoursesModel? CheckCourses(CourseDTO data)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            CoursesModel? result = null;
            try
            {
                conn.Open();
                string sql = "SELECT product.id_product, product.NAME, product.image, product.DESCRIPTION, category.id_category, category.NAME as type, product.STATUS, product.price FROM product JOIN category ON category.id_category = product.fk_id_category WHERE category.id_category = @CategoryId AND product.NAME = @CourseName";

                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@CategoryId", data.CategoryId);
                cmd.Parameters.AddWithValue("@CourseId", data.TitleCourse);

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result = new CoursesModel()
                    {
                        Id = reader.GetInt32("id_product"),
                        Name = reader.GetString("NAME"),
                        Image = reader.GetString("image"),
                        Description = reader.GetString("DESCRIPTION"),
                        CategoryId = reader.GetInt32("id_category"),
                        CategoryName = reader.GetString("type"),
                        IsActive = reader.GetBoolean("STATUS"),
                        Price = reader.GetInt32("price")
                    };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            conn.Close();

            return result;
        }
    }
}