using System;
using project_music_be.DTO;
using project_music_be.Models;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;

namespace project_music_be.Repositories
{
    public class CartRepository
    {
        private readonly string _connectionString;

        public CartRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }

        public bool AddToCart(CartsDTO cart, int UserId)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("INSERT INTO cart VALUES (DEFAULT,  @ScheduleId, @UserId,  @Selected)", conn);

                cmd.Parameters.AddWithValue("@ScheduleId", cart.ScheduleId);
                cmd.Parameters.AddWithValue("@UserId", UserId);
                cmd.Parameters.AddWithValue("@Selected", Convert.ToInt32(false));
                
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                Console.WriteLine(ex.Message);
                return false;
            }
            conn.Close();
            return true;
        }

        public List<CartsModel> GetCart(int userId)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            List<CartsModel> result = new List<CartsModel>();

            try
            {
                conn.Open();
                string sql = "SELECT cart.id_cart, cart.fk_id_user, cart.fk_id_schedule, product.image, product.NAME, schedule.schedule_date, product.price, category.name as type, cart.is_selected FROM cart JOIN schedule ON cart.fk_id_schedule = schedule.id_schedule JOIN product ON product.id_product = schedule.fk_id_product JOIN category ON category.id_category = product.fk_id_category WHERE cart.fk_id_user = @IdUser";

                MySqlCommand cmd = new MySqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@IdUser", userId);

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    result.Add(new CartsModel
                    {
                        Id = reader.GetInt32(0),
                        UserId = reader.GetInt32(1),
                        ScheduleId = reader.GetInt32(2),
                        Image = reader.GetString(3),
                        Title = reader.GetString(4),
                        Schedule = DateTime.Parse(reader.GetString(5)),
                        Price = reader.GetInt32(6),
                        Category = reader.GetString(7),
                        IsSelected = reader.GetBoolean(8),
                    });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            conn.Close();

            return result;
        }

        public bool DeleteCartById(int id)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();
                string sql = "DELETE FROM cart WHERE cart.id_cart = @Id";

                MySqlCommand cmd = new MySqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@Id", id);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                Console.WriteLine(ex.Message);
                return false;
            }
            conn.Close();

            return true;
        }

        public CartsModel CheckCartByUser(int idUser, int idScedule)
        {
            Console.WriteLine("Checking existing cart");
            MySqlConnection conn = new MySqlConnection(_connectionString);

            CartsModel? result = null;

            try
            {
                conn.Open();
                string sql = "SELECT cart.id_cart, cart.fk_id_user, cart.fk_id_schedule, product.image, product.NAME, schedule.schedule_date, product.price, category.name as type, cart.is_selected FROM cart JOIN schedule ON cart.fk_id_schedule = schedule.id_schedule JOIN product ON product.id_product = schedule.fk_id_product JOIN category ON category.id_category = product.fk_id_category WHERE cart.fk_id_user = @IdUser AND cart.fk_id_schedule = @IdSchedule";

                MySqlCommand cmd = new MySqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@IdUser", idUser);
                cmd.Parameters.AddWithValue("@IdSchedule", idScedule);

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    result = new CartsModel
                    {
                        Id = reader.GetInt32(0),
                        UserId = reader.GetInt32(1),
                        ScheduleId = reader.GetInt32(2),
                        Image = reader.GetString(3),
                        Title = reader.GetString(4),
                        Schedule = DateTime.Parse(reader.GetString(5)),
                        Price = reader.GetInt32(6),
                        Category = reader.GetString(7),
                        IsSelected = reader.GetBoolean(8),
                    };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            conn.Close();
            Console.WriteLine(result);

#pragma warning disable CS8603 // Possible null reference return.
            return result;
#pragma warning restore CS8603 // Possible null reference return.
        }

        public bool SelectCart(bool IsSelected, int id)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();
                string sql = "UPDATE cart SET is_selected=@isSelected WHERE id_cart = @Id";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@Id", id);
                cmd.Parameters.AddWithValue("@isSelected", IsSelected);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                Console.WriteLine(ex.Message);
                return false;
            }
            conn.Close();

            return true;
        }

        public bool SelectAllCart(bool IsSelected)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();
                string sql = "UPDATE cart SET is_selected=@isSelected WHERE is_selected != @isSelected";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@isSelected", IsSelected);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                Console.WriteLine(ex.Message);
                return false;
            }
            conn.Close();

            return true;
        }

        public bool IsScheduleInInvoice(int scheduleId, int userId)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();
                string sql =
                    "SELECT COUNT(*) FROM invoice WHERE fk_id_schedule = @ScheduleId AND fk_id_user = @IdUser";

                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@ScheduleId", scheduleId);
                cmd.Parameters.AddWithValue("@IdUser", userId);

                int count = Convert.ToInt32(cmd.ExecuteScalar());

                return count > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            finally
            {
                conn.Close();
            }
        }

    }
}
