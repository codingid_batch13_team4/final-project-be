using MySql.Data.MySqlClient;
using project_music_be.DTO;
using project_music_be.Models;

namespace project_music_be.Repositories
{
    public class AuthRepository
    {
        private string connStr = string.Empty;

        //Dependencies Injection

        public AuthRepository(IConfiguration configuration)
        {
            connStr = configuration.GetConnectionString("Default");
        }

        public UserModel? GetByEmailAndPassword(string email, string password, bool isRegister)
        {
            UserModel? user = null;

            MySqlConnection conn = new MySqlConnection(connStr);

            try
            {
                conn.Open();
                string sql = "";

                if (isRegister)
                {
                    sql = "SELECT * FROM users WHERE email=@Email";
                }
                else
                {
                    sql =
                        "SELECT * FROM users WHERE email=@Email AND password=@Password";
                }

                MySqlCommand cmd = new MySqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@Email", email);
                cmd.Parameters.AddWithValue("@Password", password);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    user = new UserModel
                    {
                        Id = reader.GetInt32("id_users"),
                        Name = reader.GetString("name"),
                        Email = reader.GetString("email"),
                        Role = reader.GetString("role"),
                        IsActive = reader.GetBoolean("status")
                    };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            //required
            conn.Close();
            Console.WriteLine("Done.");

            return user;
        }

        public string CreateRegister(RegisterDTO data, string password, string verificationToken)
        {
            //get connection to database
            MySqlConnection conn = new MySqlConnection(connStr);
            string errorMessage = string.Empty;
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(
                    "INSERT INTO users (id_users, name, email, password, role, status, verificationToken) VALUES (DEFAULT, @Name, @Email, @Password, @Role, @IsActive, @verificationToken)",
                    conn
                );

                cmd.Parameters.AddWithValue("@Name", data.Name);
                cmd.Parameters.AddWithValue("@Email", data.Email);
                cmd.Parameters.AddWithValue("@Password", password);
                cmd.Parameters.AddWithValue("@Role", data.Role);
                cmd.Parameters.AddWithValue("@VerificationToken", verificationToken);
                cmd.Parameters.AddWithValue("@IsActive", data.IsActive);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Console.WriteLine(ex.Message);
            }
            finally
            {
                //required
                conn.Close();
            }

            return errorMessage;
        }

        public UserModel? GetByEmail(string email)
        {
            UserModel? user = null;

            //get connection to database
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM users WHERE email=@Email", conn);

                cmd.Parameters.AddWithValue("@Email", email);

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    user = new UserModel
                    {
                        Id = reader.GetInt32("id_users"),
                        Name = reader.GetString("name"),
                        Email = reader.GetString("email"),
                        Role = reader.GetString("role")
                    };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            //required
            conn.Close();

            return user;
        }

        public bool InsertResetPasswordToken(int userId, string token)
        {
            bool isSuccess = false;

            //get connection to database
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(
                    "UPDATE users SET token_reset_pw=@Token WHERE id_users=@Id",
                    conn
                );

                cmd.Parameters.AddWithValue("@Id", userId);
                cmd.Parameters.AddWithValue("@Token", token);

                cmd.ExecuteNonQuery();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            //required
            conn.Close();

            return isSuccess;
        }

        public UserModel? GetByEmailAndResetToken(string email, string resetToken)
        {
            UserModel? user = null;

            //get connection to database
            MySqlConnection conn = new MySqlConnection(connStr);

            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(
                    "SELECT * FROM users WHERE email = @Email AND token_reset_pw = @ResetToken",
                    conn
                );

                cmd.Parameters.AddWithValue("Email", email);
                cmd.Parameters.AddWithValue("@ResetToken", resetToken);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    user = new UserModel
                    {
                        Id = reader.GetInt32("id_users"),
                        Name = reader.GetString("name"),
                        Email = reader.GetString("email"),
                        Role = reader.GetString("role")
                    };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            //required
            conn.Close();

            return user;
        }

        public bool UpdatePassword(int userId, string newPassword)
        {
            bool isSuccess = false;
            MySqlConnection conn = new MySqlConnection(connStr);
            {
                conn.Open();
                using (MySqlTransaction transaction = conn.BeginTransaction())
                {
                    try
                    {
                        MySqlCommand cmdUpdatePassword = new MySqlCommand(
                            "UPDATE users SET password=@NewPassword WHERE id_users=@Id",
                            conn,
                            transaction
                        );
                        cmdUpdatePassword.Parameters.AddWithValue("@Id", userId);
                        cmdUpdatePassword.Parameters.AddWithValue("@NewPassword", newPassword);
                        cmdUpdatePassword.ExecuteNonQuery();

                        MySqlCommand cmdRemoveToken = new MySqlCommand(
                            "UPDATE users SET token_reset_pw = '' WHERE id_users = @Id",
                            conn,
                            transaction
                        );
                        cmdRemoveToken.Parameters.AddWithValue("@Id", userId);
                        cmdRemoveToken.ExecuteNonQuery();

                        transaction.Commit();
                        isSuccess = true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Console.WriteLine("Connection Error: " + ex.Message);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }

            return isSuccess;
        }

        public bool ActivationUser(string verificationToken)
        {
            bool isSuccess = false;

            //get connection to database
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(
                    "UPDATE users SET status=true WHERE verificationToken=@Token",
                    conn
                );

                cmd.Parameters.AddWithValue("@Token", verificationToken);

                cmd.ExecuteNonQuery();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            //required
            conn.Close();

            return isSuccess;
        }

        public bool ChangeStatus(bool status, int id)
        {
            bool isSuccess = false;

            //get connection to database
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(
                    "UPDATE users SET status=@IsActive WHERE id_users=@Id",
                    conn
                );

                cmd.Parameters.AddWithValue("@IsActive", status);
                cmd.Parameters.AddWithValue("@Id", id);

                cmd.ExecuteNonQuery();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            //required
            conn.Close();

            return isSuccess;
        }

        public List<UserModel> getDataUser()
        {
            List<UserModel> user = new List<UserModel>();

            MySqlConnection conn = new MySqlConnection(connStr);

            try
            {
                conn.Open();
                string sql = "SELECT * FROM users";

                MySqlCommand cmd = new MySqlCommand(sql, conn);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    user.Add(
                        new UserModel
                        {
                            Id = reader.GetInt32("id_users"),
                            Name = reader.GetString("name"),
                            Email = reader.GetString("email"),
                            Role = reader.GetString("role"),
                            IsActive = reader.GetBoolean("status")
                        }
                    );
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            //required
            conn.Close();
            Console.WriteLine("Done.");

            return user;
        }

        public UserModel getDataUserById(int id)
        {
            UserModel user = new UserModel();

            MySqlConnection conn = new MySqlConnection(connStr);

            try
            {
                conn.Open();
                string sql = "SELECT * FROM users WHERE id_users =@Id";

                MySqlCommand cmd = new MySqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("Id", id);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    user.Id = reader.GetInt32("id_users");
                    user.Name = reader.GetString("name");
                    user.Email = reader.GetString("email");
                    user.Role = reader.GetString("role");
                    user.IsActive = reader.GetBoolean("status");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            //required
            conn.Close();
            Console.WriteLine("Done.");

            return user;
        }

        public bool UpdateUser(RegisterDTO data, int id)
        {
            bool isSuccess = false;

            //get connection to database
            MySqlConnection conn = new MySqlConnection(connStr);

            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(
                    "UPDATE users SET name=@Name, email=@Email, role=@Role, status=@IsActive WHERE id_users=@Id",
                    conn
                );

                cmd.Parameters.AddWithValue("@Name", data.Name);
                cmd.Parameters.AddWithValue("@Email", data.Email);
                cmd.Parameters.AddWithValue("@Role", data.Role);
                cmd.Parameters.AddWithValue("@IsActive", data.IsActive);
                cmd.Parameters.AddWithValue("@Id", id);
                cmd.ExecuteNonQuery();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            //required
            conn.Close();

            return isSuccess;
        }
    }
}
