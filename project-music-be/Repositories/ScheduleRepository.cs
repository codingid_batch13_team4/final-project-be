using project_music_be.DTO;
using project_music_be.Models;
using MySql.Data.MySqlClient;

namespace project_music_be.Repositories
{
    public class ScheduleRepository
    {
        private readonly string _connectionString;
        public ScheduleRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }

        public bool InsertSchedule(ScheduleDTO data)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();
                string sql = "INSERT INTO schedule VALUES (DEFAULT,@DateTime, @IdProduct)";

                MySqlCommand cmd = new MySqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@DateTime", data.Schedule);
                cmd.Parameters.AddWithValue("@IdProduct", data.ProductId);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                Console.WriteLine(ex.Message);
                return false;
            }
            conn.Close();

            return true;
        }

        public List<ScheduleModel> GetSchedule()
        {

            MySqlConnection conn = new MySqlConnection(_connectionString);
            List<ScheduleModel> result = new List<ScheduleModel>();

            try
            {
                conn.Open();
                string sql = "SELECT id_schedule, schedule_date, fk_id_product, product.name FROM schedule JOIN product ON product.id_product = schedule.fk_id_product";
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    int id = reader.GetInt32("id_schedule");
                    DateTime time = DateTime.Parse(reader.GetString("schedule_date"));
                    string name = reader.GetString("name");

                    result.Add(new ScheduleModel
                    {
                        Id = id,
                        Date = time,
                        TitleCourse = name
                    });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            conn.Close();

            return result;
        }

        public ScheduleModel? CheckSchedule(ScheduleDTO data)
        {

            MySqlConnection conn = new MySqlConnection(_connectionString);
            ScheduleModel? result = null;

            try
            {
                conn.Open();
                string sql = "SELECT id_schedule, schedule_date, fk_id_product, product.name FROM schedule JOIN product ON product.id_product = schedule.fk_id_product WHERE schedule_date = @Schedule AND fk_id_product = @CourseId";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@Schedule", data.Schedule);
                cmd.Parameters.AddWithValue("@CourseId", data.ProductId);

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result = new ScheduleModel()
                    {
                        Id = reader.GetInt32("id_schedule"),
                        Date = DateTime.Parse(reader.GetString("schedule_date")),
                        TitleCourse = reader.GetString("name")
                    };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            conn.Close();

            return result;
        }
    }
}