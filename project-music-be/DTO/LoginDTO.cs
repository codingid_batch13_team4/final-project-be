using System.ComponentModel;

namespace project_music_be.DTO
{
    public class LoginDTO
    {
        // [DefaultValue("neriy@gmail.com")]
        public string Email { get; set; } = string.Empty;
        // [DefaultValue("12345678")]
        public string Password { get; set; } = string.Empty;
    }
}