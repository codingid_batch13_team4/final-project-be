namespace project_music_be.DTO
{
    public class CourseDTO
    {
        public string? TitleCourse { get; set; }
        public string? DescriptionCourse { get; set;}
        public int? CategoryId { get; set;}
        public int? Price { get; set;}
        public IFormFile? Image { get; set;}
        public bool? IsActive { get; set;}
    }
}