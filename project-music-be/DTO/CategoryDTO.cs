namespace project_music_be.DTO
{
    public class CategoryDTO
    {
        public string? Name {get; set; }
        public string? Description {get; set; }
        public IFormFile? Image { get; set; }
        public bool Status { get; set; }
    }
}