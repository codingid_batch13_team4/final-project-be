namespace project_music_be.DTO
{
    public class PaymentDTO
    {
        public string? Name { get; set; }
        public IFormFile? Image { get; set; }
        public bool Status { get; set; }
    }
}