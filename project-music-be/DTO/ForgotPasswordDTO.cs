using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;

namespace project_music_be.DTO
{
    public class ForgotPasswordDTO
    {
        public string Email { get; set; } = string.Empty;
        [DefaultValue("http://localhost:5173/new-password/?email=")]
        public string Link { get; set; } = string.Empty;
    }
}