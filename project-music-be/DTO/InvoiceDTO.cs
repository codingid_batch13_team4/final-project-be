using project_music_be.Models;

namespace project_music_be.DTO
{
    public class InvoiceDTO
    {
        public DateTime CreateAt { set; get; }
        public int PaymentId { set; get;}
    }
}