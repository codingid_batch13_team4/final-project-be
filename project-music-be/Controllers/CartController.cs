using project_music_be.Models;
using project_music_be.DTO;
using project_music_be.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace project_music_be.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CartController : ControllerBase
    {
        private readonly CartRepository _cartRepository;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public CartController(CartRepository cart, IWebHostEnvironment webHostEnvironment)
        {
            _cartRepository = cart;
            _webHostEnvironment = webHostEnvironment;
        }

        [Authorize]
        [HttpPost]
        public IActionResult CreateCart([FromBody] CartsDTO data)
        {
            string userId = User.FindFirstValue(ClaimTypes.Sid);

            CartsModel checkByUser = _cartRepository.CheckCartByUser(int.Parse(userId), data.ScheduleId);

            if (checkByUser != null)
            {
                return BadRequest(new
                {
                    status = "Sudah ada dalam cart",
                });
            }

            bool checkByInvoice = _cartRepository.IsScheduleInInvoice(data.ScheduleId, int.Parse(userId));

            if (checkByInvoice)
            {
                return BadRequest(new
                {
                    status = "Course already in Invoice",
                });
            }

            bool cart = _cartRepository.AddToCart(data, int.Parse(userId));

            if (cart)
            {
                return Ok(new
                {
                    status = "Success",
                });
            }

            return BadRequest(new
            {
                status = "Failed",
            });
        }

        // [HttpGet]
        // public IActionResult GetAllCart()
        // {
        //     List<CartsModel> result = _cartRepository.GetAllCart();

        //     return Ok(result);

        // }

        [Authorize]
        [HttpGet()]
        public IActionResult GetCart()
        {
            string userId = User.FindFirstValue(ClaimTypes.Sid);
            List<CartsModel> result = _cartRepository.GetCart(int.Parse(userId));

            return Ok(result);

        }

        [Authorize]
        [HttpPut("cartSelected/{id}")]
        public IActionResult SelectCart([FromBody] bool IsSelected, int id)
        {
            bool cart = _cartRepository.SelectCart(IsSelected, id);
            if (cart)
            {
                return Ok(new
                {
                    status = "Success",
                });
            }

            return BadRequest(new
            {
                status = "Failed",
            });
        }

        [Authorize]
        [HttpPut("selectAll")]
        public IActionResult SelectAllCart([FromBody] bool IsSelected)
        {
            bool cart = _cartRepository.SelectAllCart(IsSelected);
            if (cart)
            {
                return Ok(new
                {
                    status = "Success",
                });
            }

            return BadRequest(new
            {
                status = "Failed",
            });
        }

        [Authorize]
        [HttpDelete("{id}")]
        public IActionResult DeleteCartById(int id)
        {
            return Ok(_cartRepository.DeleteCartById(id));
        }
    }
    
}