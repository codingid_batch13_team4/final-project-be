using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using project_music_be.DTO;
using project_music_be.Models;
using project_music_be.Repositories;

namespace project_music_be.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CoursesController : ControllerBase
    {
        private readonly CoursesRepository _coursesRepository;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public CoursesController(CoursesRepository courses, IWebHostEnvironment webHostEnvironment)
        {
            _coursesRepository = courses;
            _webHostEnvironment = webHostEnvironment;
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        public async Task<IActionResult> CreateCourse([FromForm] CourseDTO data)
        {
            try
            {
                IFormFile image = data.Image!;

                var ext = Path.GetExtension(image.FileName).ToLowerInvariant();
                string fileName = Guid.NewGuid().ToString() + ext;
                string uploadDir = "uploads";
                string physicalPath = $"wwwroot/{uploadDir}";
                var filePath = Path.Combine(
                    _webHostEnvironment.ContentRootPath,
                    physicalPath,
                    fileName
                );
                using var stream = System.IO.File.Create(filePath);
                await image.CopyToAsync(stream);
                string fileUrlPath = $"{uploadDir}/{fileName}";

                CoursesModel? result = _coursesRepository.CheckCourses(data);
                if (result != null)
                {
                    return BadRequest(new { status = "Course Sudah Ada", });
                }

                bool course = _coursesRepository.InsertCourse(data, fileUrlPath);

                if (course)
                {
                    return Ok(new { status = "Success", });
                }

                return BadRequest(new { status = "Failed", reason = "Course insertion failed.", });
            }
            catch (Exception ex)
            {
                // Log the exception
                Debug.WriteLine($"Exception: {ex.Message}");
                return BadRequest(
                    new { status = "Failed", reason = "An unexpected error occurred.", }
                );
            }
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPut("Update/{id}")]
        public async Task<IActionResult> UpdateData([FromForm] CourseDTO data, int id)
        {
            CourseDetailModel courseById = _coursesRepository.GetDetailCourse(id);

            string fileUrlPath = courseById.Image!;
            data.DescriptionCourse ??= courseById.Description;
            data.TitleCourse ??= courseById.Name;
            data.Price ??= courseById.Price;
            data.CategoryId ??= courseById.CategoryId;

            if (data.Image != null)
            {
                IFormFile image = data.Image!;

                var ext = Path.GetExtension(image.FileName).ToLowerInvariant();
                string fileName = Guid.NewGuid().ToString() + ext;

                string uploadDir = "uploads";
                string physicalPath = $"wwwroot/{uploadDir}";
                var filePath = Path.Combine(
                    _webHostEnvironment.ContentRootPath,
                    physicalPath,
                    fileName
                );
                using var stream = System.IO.File.Create(filePath);
                await image.CopyToAsync(stream);

                fileUrlPath = $"{uploadDir}/{fileName}";
            }

            bool product = _coursesRepository.UpdateCourse(data, fileUrlPath, id);

            if (product)
            {
                return Ok(new { status = "Success", });
            }

            return BadRequest(new { status = "Failed", });
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPut("Active")]
        public IActionResult UpdateStatus([FromQuery] bool isActive, int id)
        {
            bool course = _coursesRepository.UpdateStatus(isActive, id);
            if (course)
            {
                return Ok(new { status = "Success", });
            }

            return BadRequest(new { status = "Failed", });
        }

        [HttpGet]
        public IActionResult GetListCourses([FromQuery] int? limit, int? categoryId, int? courseId)
        {
            List<CoursesModel> result = _coursesRepository.GetCourses(limit, categoryId, courseId);
            return Ok(result);
        }

        [HttpGet("Detail/{id}")]
        public IActionResult GetDetailCourse(int id)
        {
            CourseDetailModel result = _coursesRepository.GetDetailCourse(id);
            return Ok(result);
        }

        [Authorize(Roles = "ADMIN")]
        [HttpDelete("{id}")]
        public IActionResult DeleteCourse(int id)
        {
            return Ok(_coursesRepository.DeleteCourse(id));
        }
    }
}
