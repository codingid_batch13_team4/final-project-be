using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using project_music_be.DTO;
using project_music_be.Helper;
using project_music_be.Models;
using project_music_be.Repositories;
using Microsoft.AspNetCore.Authorization;
using Org.BouncyCastle.Crypto.Engines;

namespace project_music_be.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly AuthRepository _authRepository;
        public AuthController(AuthRepository authRepository)
        {
            _authRepository = authRepository;
        }


        [HttpPost("Login")]
        public IActionResult Login([FromBody] LoginDTO data)
        {
            string hashedPassword = PasswordHelper.EncryptPassword(data.Password);

            UserModel? user = _authRepository.GetByEmailAndPassword(data.Email, hashedPassword, false);

            if (user == null)
            {
                return NotFound();
            }

            if (user.IsActive == false)
            {
                return BadRequest(new
                {
                    status = "Account is InActive",
                });
            }

            //create token
            string token = JWTHelper.Generate(user.Id, user.Role);

            return Ok(new
            {
                tokenUser = token,
                role = user.Role
            });
        }

        [HttpPost("Register")]
        public async Task<IActionResult> Register([FromBody] RegisterDTO data, string? link)
        {
            string hashedPassword = PasswordHelper.EncryptPassword(data.Password);
            string verificationToken = Guid.NewGuid().ToString();

            UserModel? user = _authRepository.GetByEmailAndPassword(data.Email, hashedPassword, true);

            // bool register = false;

            if (user != null)
            {
                return BadRequest(new
                {
                    status = "Email Already Taken",
                });
            }


            string errorMessage = _authRepository.CreateRegister(data, hashedPassword, verificationToken);
            if (!string.IsNullOrEmpty(errorMessage))
            {
                return Problem("Error when registering user");

            }

            //send email
            // string linkEmail =  $@"
            // Just click the link below to activate your account<br/>
            // <a href='http://localhost:5173/verify/{verificationToken}'>Verify My Account</a>
            // ";  

            string verifLink = link + verificationToken;
            Console.WriteLine(verifLink);

            await MailHelper.Send(data.Email, "Active Account", "Hello " + data.Name + ", Please press this link to active your account " + verifLink);
            return Ok();
        }

        [HttpPost("ForgotPassword")]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordDTO data)
        {

            UserModel? user = _authRepository.GetByEmail(data.Email);
            if (user == null)
            {
                return NotFound(new
                {
                    status = "Account is Not Found",
                });
            }

            string key = DateTime.UtcNow.Ticks.ToString() + data.Email;
            string resetToken = PasswordHelper.EncryptPassword(key);

            //update reset token
            bool isSuccess = _authRepository.InsertResetPasswordToken(user.Id, resetToken);

            if (!isSuccess)
            {
                return Problem();
            }

            string resetLink = data.Link + data.Email + "&token=" + resetToken;

            await MailHelper.Send(data.Email, "Forgot password", "Hello " + user.Email + ", there was recently a request to change the password for your account." + "If you requested this password change, please reset your password here: " + resetLink);

            return Ok();

        }

        [HttpPost("ResetPassword")]
        public IActionResult ResetPassword([FromBody] ResetPasswordDTO data)
        {
            //check if email valid
            UserModel? user = _authRepository.GetByEmailAndResetToken(data.Email, data.Token);
            if (user == null)
            {
                return NotFound(new
                {
                    status = "Account Not Found"
                });
            }

            string hashedPassword = PasswordHelper.EncryptPassword(data.NewPassword);

            bool isResetSuccess = _authRepository.UpdatePassword(user.Id, hashedPassword);

            if (!isResetSuccess)
            {
                return BadRequest(new
                {
                    status = "Reset Unsuccessful"
                });
            }

            return Ok(new
            {
                status = "Success"
            });
        }

        [HttpPut]
        public IActionResult AuthActive([FromQuery] string verificationToken)
        {
            bool activate = _authRepository.ActivationUser(verificationToken);

            if (!activate)
            {
                return NotFound(new
                {
                    status = "Failed"
                });
            }
            return Ok(new
            {
                status = "Success"
            });
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPut("{id}")]
        public IActionResult ChangeStatus(int id, [FromQuery] bool status)
        {
            bool active = _authRepository.ChangeStatus(status, id);

            if (!active)
            {
                return NotFound(new
                {
                    status = "Failed"
                });
            }

            return Ok(new
            {
                status = "Success"
            });
        }

        [Authorize(Roles = "ADMIN")]
        [HttpGet]
        public IActionResult GetDataUser()
        {
            List<UserModel> userData = _authRepository.getDataUser();

            return Ok(userData);
        }

        [Authorize(Roles = "ADMIN")]
        [HttpGet("{id}")]
        public IActionResult GetDataUserById(int id)
        {
            UserModel userData = _authRepository.getDataUserById(id);

            return Ok(userData);
        }

        [HttpPatch("{id}")]
        public IActionResult UpdateDataUser(int id, [FromBody] RegisterDTO data)
        {
            UserModel userData = _authRepository.getDataUserById(id);

            data.Name = data.Name == null || data.Name == "" ? userData.Name : data.Name;
            data.Email = data.Email == null || data.Email == "" ? userData.Email : data.Email;
            data.Role = data.Role == null || data.Role == "" ? userData.Role : data.Role;

            bool active = _authRepository.UpdateUser(data, id);

            if (!active)
            {
                return NotFound(new
                {
                    status = "Failed"
                });
            }

            return Ok(new
            {
                status = "Success"
            });
        }
    }
}