using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using project_music_be.DTO;
using project_music_be.Models;
using project_music_be.Repositories;

namespace project_music_be.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ScheduleController : ControllerBase
    {
        private readonly ScheduleRepository _scheduleRepository;

        public ScheduleController(ScheduleRepository schedule)
        {
            _scheduleRepository = schedule;
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        public IActionResult InsertSchedule([FromForm] ScheduleDTO data)
        {
            ScheduleModel? result = _scheduleRepository.CheckSchedule(data);
            if (result != null)
            {
                return BadRequest(new { status = "Jadwal Sudah Ada", });
            }

            bool schedule = _scheduleRepository.InsertSchedule(data);

            if (schedule)
            {
                return Ok(new { status = "Success", });
            }

            return BadRequest(new { status = "Failed", });
        }

        [HttpGet]
        public IActionResult GetSchedule()
        {
            List<ScheduleModel> result = _scheduleRepository.GetSchedule();

            return Ok(result);
        }
    }
}
