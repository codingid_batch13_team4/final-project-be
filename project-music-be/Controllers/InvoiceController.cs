using project_music_be.Models;
using project_music_be.DTO;
using project_music_be.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace project_music_be.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class InvoiceController : ControllerBase
    {
        private readonly InvoiceRepository _invoiceRepository;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public InvoiceController(InvoiceRepository invoice, IWebHostEnvironment webHostEnvironment)
        {
            _invoiceRepository = invoice;
            _webHostEnvironment = webHostEnvironment;
        }

        [Authorize]
        [HttpPost]
        public IActionResult InsertInvoice([FromBody] InvoiceDTO data)
        {
            string userId = User.FindFirstValue(ClaimTypes.Sid);

            List<CheckInvoiceModel> DataInvoice = _invoiceRepository.GetUnlistedCourse(data, int.Parse(userId));

            if (!DataInvoice.Any())
            {
                return BadRequest(new
                {
                    status = "Sudah Pernah Order",
                });
            }

            bool invoice = _invoiceRepository.InsertInvoice(DataInvoice, data, int.Parse(userId));

            if (invoice)
            {
                return Ok(new
                {
                    status = "Success",
                });
            }
            return Problem("Failed to Checkout");
        }

        [Authorize]
        [HttpPost("Detail")]
        public IActionResult InsertDirectInvoice([FromBody] InvoiceDTO data, int ScheduleId)
        {
            string userId = User.FindFirstValue(ClaimTypes.Sid);

            bool checkByUser = _invoiceRepository.IsScheduleInInvoice(ScheduleId, int.Parse(userId));

            if (checkByUser)
            {
                return BadRequest(new
                {
                    status = "Sudah Pernah Order",
                });
            }

            bool checkByCart = _invoiceRepository.IsScheduleInCart(ScheduleId, int.Parse(userId));

            if (checkByCart)
            {
                return BadRequest(new
                {
                    status = "Sudah Ada dalam cart",
                });
            }

            bool invoice = _invoiceRepository.InsertDirectInvoice(ScheduleId, data, int.Parse(userId));

            if (invoice)
            {
                return Ok(new
                {
                    status = "Success",
                });
            }
            return Problem("Failed to Checkout");
        }

        [Authorize]
        [HttpGet()]
        public IActionResult GetInvoiceById()
        {
            string userId = User.FindFirstValue(ClaimTypes.Sid);
            List<InvoiceModel> invoices = _invoiceRepository.GetInvoiceById(int.Parse(userId));

            return Ok(invoices);
        }

        [Authorize(Roles = "ADMIN")]
        [HttpGet("AdminDetail")]
        public IActionResult GetDetailInvoiceAdmin([FromQuery] string? number)
        {
            string userId = User.FindFirstValue(ClaimTypes.Sid);
            DetailInvoiceModel detail = _invoiceRepository.GetDetailInvoiceAdmin(number!);

            return Ok(detail);
        }

        [Authorize]
        [HttpGet("Detail")]
        public IActionResult GetDetailInvoice([FromQuery] string? number)
        {
            string userId = User.FindFirstValue(ClaimTypes.Sid);
            DetailInvoiceModel detail = _invoiceRepository.GetDetailInvoice(number!, int.Parse(userId));

            return Ok(detail);
        }

        [Authorize]
        [HttpGet("MyClass")]
        public IActionResult MyClass()
        {
            string userId = User.FindFirstValue(ClaimTypes.Sid);
            List<UserClassModel> myclass = _invoiceRepository.GetMyClass(int.Parse(userId));

            return Ok(myclass);
        }

        [Authorize(Roles = "ADMIN")]
        [HttpGet("Admin")]
        public IActionResult GetInvoiceAll()
        {
            List<InvoiceModel> invoices = _invoiceRepository.GetInvoiceAll();

            return Ok(invoices);
        }
    }
}