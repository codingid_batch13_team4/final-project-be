using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using project_music_be.DTO;
using project_music_be.Models;
using project_music_be.Repositories;

namespace project_music_be.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoryController : ControllerBase
    {
        private readonly CategoryRepository _categoryRepository;
        private readonly IWebHostEnvironment _webHostEnvirontment;

        public CategoryController(
            CategoryRepository category,
            IWebHostEnvironment webHostEnvironment
        )
        {
            _categoryRepository = category;
            _webHostEnvirontment = webHostEnvironment;
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        public async Task<IActionResult> CreateCategory([FromForm] CategoryDTO data)
        {
            IFormFile image = data.Image!;

            var ext = Path.GetExtension(image.FileName).ToLowerInvariant();
            string fileName = Guid.NewGuid().ToString() + ext;

            string uploadDir = "uploads";
            string physicalPath = $"wwwroot/{uploadDir}";
            var filePath = Path.Combine(
                _webHostEnvirontment.ContentRootPath,
                physicalPath,
                fileName
            );
            using var stream = System.IO.File.Create(filePath);
            await image.CopyToAsync(stream);

            string fileUrlPath = $"{uploadDir}/{fileName}";

            CategoryModel? result = _categoryRepository.CheckCategory(data);
            if (result != null)
            {
                return BadRequest(new { status = "Category Sudah Ada", });
            }

            bool category = _categoryRepository.InsertCategory(data, fileUrlPath);
            if (category)
            {
                return Ok(new { status = "Success", });
            }

            return BadRequest(new { status = "Failed", });
        }

        [HttpGet]
        public IActionResult GetAll([FromQuery] bool? status)
        {
            List<CategoryModel> result = _categoryRepository.GetListCategory(status);

            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            CategoryModel result = _categoryRepository.GetCategoryId(id);

            return Ok(result);
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCategoryById([FromForm] CategoryDTO data, int id)
        {
            CategoryModel categoryById = _categoryRepository.GetCategoryId(id);

            string fileUrlPath = categoryById.Image!;
            data.Name ??= categoryById.Name;
            data.Description ??= categoryById.Description;

            if (data.Image != null)
            {
                IFormFile image = data.Image;

                var ext = Path.GetExtension(image.FileName).ToLowerInvariant();
                string fileName = Guid.NewGuid().ToString() + ext;

                string uploadDir = "uploads";
                string physicalPath = $"wwwroot/{uploadDir}";
                var filePath = Path.Combine(
                    _webHostEnvirontment.ContentRootPath,
                    physicalPath,
                    fileName
                );
                using var stream = System.IO.File.Create(filePath);
                await image.CopyToAsync(stream);

                fileUrlPath = $"{uploadDir}/{fileName}";
            }

            bool category = _categoryRepository.UpdateCategory(data, fileUrlPath, id);
            if (category)
            {
                return Ok(new { status = "Success", });
            }

            return BadRequest(new { status = "Failed", });
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPut]
        public IActionResult UpdateStatusCategory([FromQuery] int id, bool status)
        {
            bool category = _categoryRepository.UpdateStatusCategory(id, status);
            if (category)
            {
                return Ok(new { status = "Success" });
            }

            return BadRequest(new { status = "Failed" });
        }
    }
}
